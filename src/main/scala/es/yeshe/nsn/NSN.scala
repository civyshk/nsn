package es.yeshe.nsn

import scala.annotation.tailrec
import scala.collection.immutable.TreeSet

/**
 * NSN (Non Summable Numbers) defines a series of integers in which each element
 * can not be obtained as the sum of any combination of numbers in the same series.
 *
 * Initialize a series with M initial values as NSN(1, 3, ...)
 * and get the series of M + N elements with NSN(...).grow(N)
 */
object NSN{

  /**
   * Test if some set of initial numbers can be used to build a NSN
   *
   * @param numbers
   */
  def numbersAreValid(numbers: Seq[Int]): Boolean = NSN.getFirstWrong(numbers: _*).isEmpty

  /**
   * Given a not valid set of initial numbers, get the first that can be obtained as the sum
   * of some combination of other numbers
   *
   * @param numbers
   */
  def getFirstWrong(numbers: Int*): Option[Int] = {
    @tailrec
    def aux(processed: Seq[Int], left: Seq[Int]): Option[Int] = {
      (processed, left) match {
        case (p, headLeft :: tailLeft) if NSN(p: _*).canGrowWith(headLeft) => aux(p :+ headLeft, tailLeft)
        case (_, headLeft :: _) => Some(headLeft)
        case _ => None
      }
    }
    aux(Nil, numbers.toList)
  }

  /**
   * Initialize a NSN with some initial values
   *
   * @param numbers Seq of Int that must be valid - [[NSN.numbersAreValid()]]
   * @return
   */
  def apply(numbers: Int*): NSN = {
    assert(numbersAreValid(numbers), "")

    val allSummed = for {
      i <- numbers.indices.map(_ + 1)
      iCombination <- numbers.combinations(i)
      summed = iCombination.sum
    } yield summed

    val allAsSet = TreeSet.from(allSummed)

    new NSN(numbers.sorted.reverse, allAsSet)
  }
}

private[nsn] class NSN(val numbers: Seq[Int], val added: TreeSet[Int]){

  def canGrowWith(next: Int): Boolean = !added.contains(next)

  def grow: NSN = {
    val next = LazyList.from(numbers.head + 1).filter(canGrowWith).head
    val nextNumbers = next +: numbers
    val nextAdded = added.map(_ + next) + next ++ added
    new NSN(nextNumbers, nextAdded)
  }

  def grow(n: Int): NSN = {
    @tailrec
    def aux(acc: NSN, n: Int): NSN = if(n == 0) acc else aux(acc.grow, n - 1)
    aux(this, n)
  }

  override def toString: String = numbers.reverse.mkString(", ")
  def head: Int = numbers.head
}
