package es.yeshe.nsn

object Main extends App {

  var length = 10
  var initNumbers: Seq[Int] = _

  println(s"Get $length numbers starting from 1")
  println(NSN(1) grow length)

  initNumbers = Seq(2, 3, 7)
  println(s"Get $length more numbers starting with some custom numbers: ${initNumbers.mkString(", ")}")
  println(NSN(initNumbers: _*) grow length)

  println("Not all custom numbers are valid")
  initNumbers = Seq(1, 2, 4, 8, 15)
  println(NSN.numbersAreValid(initNumbers))

  println(s"Because ${initNumbers.last} is wrong (it can be calculated as ${initNumbers.dropRight(1).mkString(" + ")})")
  println(NSN.getFirstWrong(initNumbers: _*))

  println("So trying to build a series with these numbers will fail with an exception")
  try{
    println(NSN(initNumbers: _*) grow length)
  }catch{
    case _: AssertionError => println("Failed NSN")
  }

}
